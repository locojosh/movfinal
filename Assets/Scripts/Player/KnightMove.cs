﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(Rigidbody2D))]
public class KnightMove : MonoBehaviour
{
    public float velocity;
    public float jumpForce;
    Rigidbody2D rb;
    Animator animator;
    private float direction;
    public GyroControl gyroControl;
    private void Awake() 
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }
    private void Update() 
    {
        GyroMove();
        if(direction != 0) Move();
    }
    private void GyroMove()
    {
        if(gyroControl.gyroEulerAngles.y > 10) direction = 1;
        else if(gyroControl.gyroEulerAngles.y < -10) direction = -1;
    }
    private void Move()
    { 
        Vector2 newVelocity = new Vector2(direction*velocity, rb.velocity.y);
        
        rb.velocity = newVelocity;
        AudioManagerGO.Instance.PlayFootstep();
    }    
    public void Jump()
    {
        Vector2 newJump = new Vector2(rb.velocity.x, jumpForce);

        rb.velocity = newJump;
    }
    private void Attack()
    {
        animator.SetTrigger("attack");
        AudioManagerGO.Instance.Play("attack");
    }
    //input system
    public void Move(InputAction.CallbackContext context)
    {
        float dir = context.ReadValue<float>(); 
        
        if(dir > 0.2f || dir < -0.2f)
        {
            direction = dir > 0? 1f: -1f;
            animator.SetBool("run", true);
            transform.localScale = direction > 0 ? new Vector3(1,1,1) : new Vector3(-1,1,1);
            //changeSize.ChangeToSmall();
        }
        else
        {
            direction = 0f;
            animator.SetBool("run", false);
            //changeSize.ChangeToNormal();
        }     
    }
    public void OnAttack(InputAction.CallbackContext context)
    {
        Attack();
    }
}
