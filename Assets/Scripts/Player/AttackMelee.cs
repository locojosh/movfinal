﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackMelee : MonoBehaviour
{
    private float nextPunch;
    public float firerate;

    public Transform attackPos;
    public LayerMask whatIsEnemies;
    public float attackRange;
    public int damage;
    public bool isAttack = false;

    public GameObject attackEffect;

    Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {/*
        if (Input.GetKeyDown(KeyCode.E) && Time.time > nextPunch)
        {
            nextPunch = Time.time + firerate;
            anim.SetBool("isAttack2", true);
            Collider2D[] enemiesToDamage = Physics2D.OverlapCircleAll(attackPos.position, attackRange, whatIsEnemies);
            for (int i = 0; i < enemiesToDamage.Length; i++)
            {
                enemiesToDamage[i].GetComponent<EnemyGuy>().TakeDamage(damage);
            }
            var instantiatedObj = (GameObject)Instantiate(attackEffect, new Vector2(attackPos.position.x - 0.2f , attackPos.position.y + -0.05f) , Quaternion.identity);
            StartCoroutine(RemovePunchEffect(instantiatedObj));
        }
        if (Input.GetKeyUp(KeyCode.E))
        {
            anim.SetBool("isAttack2", false);
        }*/
    }
    public void OnTap_Attack()
    {
        if(Time.time > nextPunch)
        {
            nextPunch = Time.time + firerate;
            anim.SetBool("attack", true);
            Collider2D[] enemiesToDamage = Physics2D.OverlapCircleAll(attackPos.position, attackRange, whatIsEnemies);
            for (int i = 0; i < enemiesToDamage.Length; i++)
            {
                enemiesToDamage[i].GetComponent<EnemyGuy>().TakeDamage(damage);
            }
            var instantiatedObj = (GameObject)Instantiate(attackEffect, new Vector2(attackPos.position.x , attackPos.position.y + -0.05f) , Quaternion.identity);
            StartCoroutine(RemovePunchEffect(instantiatedObj));
        }
    }

    IEnumerator RemovePunchEffect(GameObject instantiatedObj) {
        yield return new WaitForSeconds(0.3f);
        Destroy(instantiatedObj);
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(attackPos.position, attackRange);
    }
}
