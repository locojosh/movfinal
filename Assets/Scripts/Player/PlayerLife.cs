using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLife : MonoBehaviour
{
    #region SINGLETON
    //Turn this class into a Singleton
    private static PlayerLife instance;
    public static PlayerLife Instance
    {
        get
        {
            if(instance == null)
            Debug.LogError("PlayerLife is NULL");

            return instance; 
        }
    } 
    #endregion

    public GameOverPanel panelGameOver;

    private void Awake() {
        instance = this;
    }
    public int life = 100;
    public GameObject lifeBar;

    public void GetDamage(int amount)
    {
        life -= amount;
        if(life <0) life = 0;

        lifeBar.transform.localScale = new Vector3((float)life/100f, 1,1);

        if(life <= 0)
        {
            Die();
        }
    }

    public void Die()
    {
        panelGameOver.OnGameOver();
    }
}
