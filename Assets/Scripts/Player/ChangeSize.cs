using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeSize : MonoBehaviour
{
    [SerializeField] float normalSize;
    public float changeAmount = 0.05f;

    private void Start() {
        normalSize = transform.localScale.x;
    }
    public void GrowBigger()
    {
        if(transform.localScale.x <= 0.7f)
        {
            Vector3 newSize = new Vector3(transform.localScale.x + changeAmount, transform.localScale.y + changeAmount, normalSize);
            transform.localScale = newSize;
        }        
    }
    public void GrowSmaller()
    {
        if(transform.localScale.x >=0.1f)
        {
            Vector3 newSize = new Vector3(transform.localScale.x - changeAmount, transform.localScale.y - changeAmount, normalSize);
            transform.localScale = newSize;
        }        
    }
    public void ChangeToNormal()
    {
        transform.localScale = new Vector3(normalSize, normalSize, normalSize);
    }
}
