using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileControl : MonoBehaviour
{
    public float lifeT = 0.6f;
    public int damage = 10;

    private void OnEnable() 
    {
        Invoke("Disable", lifeT);
    }
    private void Disable()
    {
        gameObject.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.CompareTag("Enemy"))
        {
            EnemyGuy enemyGuy = other.GetComponent<EnemyGuy>();
            enemyGuy.TakeDamage(damage);
            if(enemyGuy.life > 0)
            gameObject.SetActive(false);
        }
    }
}
