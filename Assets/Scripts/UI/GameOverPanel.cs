using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverPanel : MonoBehaviour
{
    public GameObject panelGameOver;

    private void Start() {
        panelGameOver.SetActive(false);
    }
    public void OnGameOver()
    {
        panelGameOver.SetActive(true);
    }
    public void OnClickRestart()
    {
        SceneManager.LoadScene("Nivel1");
    }
}
