using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanelPause : MonoBehaviour
{
    public GameObject panelPause;

    public Toggle toggleSound;
    public Slider sliderMusic;

    private void Start() 
    {
        panelPause.SetActive(false);

        AudioManagerGO.Instance.ToggleOnAllSounds(PlayerPrefs.GetInt("sound", 1) == 1 ? true : false);
        AudioManagerGO.Instance.SetVolume("back", PlayerPrefs.GetFloat("music", 0.5f));
    }

    public void OnBtnClick_Pause()
    {
        panelPause.SetActive(!panelPause.activeSelf);
    }

    public void OnToggleChange_Sound()
    {        
        AudioManagerGO.Instance.ToggleOnAllSounds(toggleSound.isOn);
        PlayerPrefs.SetInt("sound", toggleSound.isOn ? 1 : 0);
    }
    public void OnSliderChanged_Music()
    {
        AudioManagerGO.Instance.SetVolume("back", sliderMusic.value);
        PlayerPrefs.SetFloat("music", sliderMusic.value);
    }
}
