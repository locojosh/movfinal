using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGuy : MonoBehaviour
{
    public int life = 100;
    private Animator anim;
    public EnemyPatrol sEnemyPatrol;
    public Transform lifeBar;
    public GameObject attackEffect; 
    public GameObject coinPrefab;

    void Start()
    {
        anim = GetComponent<Animator>();
        lifeBar.localScale = new Vector3((float)life/100f, 1, 1);
    }
    public void TakeDamage(int damage)
    {
        anim.SetTrigger("hurt");
        life -= damage;

        var instantiatedObj = (GameObject)Instantiate(attackEffect, new Vector2(transform.position.x , transform.position.y + -0.05f) , Quaternion.identity);
            StartCoroutine(RemovePunchEffect(instantiatedObj));
            
        if(life< 0) 
        {
            life = 0;
        }
        
        lifeBar.localScale = new Vector3((float)life/100f, 1, 1);

        if(life <= 0)
            Die();
    }
    public void Die()
    {
        anim.SetTrigger("die");
        sEnemyPatrol._isAlive = false;
        GetComponent<Rigidbody2D>().simulated = false;
        GetComponent<BoxCollider2D>().enabled = false;

        Invoke("InstantiateCoin", 0.75f);
    }
    void InstantiateCoin()
    {
        Vector3 pos = transform.position;
        pos.y -= 2.0f;
        Instantiate(coinPrefab, pos, Quaternion.identity);
    }
    IEnumerator RemovePunchEffect(GameObject instantiatedObj) {
        yield return new WaitForSeconds(0.5f);
        Destroy(instantiatedObj);
    }
}
