using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour
{
    public float lifeT = 0.6f;
    public int damage = 10;

    private void OnEnable() {
        Invoke("Disable", lifeT);
    }
    private void Disable()
    {
        //gameObject.SetActive(false);
        Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.CompareTag("Player"))
        {
            PlayerLife.Instance.GetDamage(damage);
            
            Disable();
        }
        else if(other.CompareTag("Wall"))
        {
            Disable();
        }
    }
}
