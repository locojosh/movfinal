﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UICollectables : MonoBehaviour
{
    public static UICollectables Instance;

    public TextMeshProUGUI txtCoins;
    public TextMeshProUGUI txtGems;

    private void Awake() 
    {
        Instance = this;
    }
    private void Start() 
    {
        Player.Instance.UpdateUI();
    }
    
    public void UpdateUI(int coins, int gems)
    {
        txtCoins.text = coins +"";
        txtGems.text = gems +"";
    }
}
