using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoNextLevel : MonoBehaviour
{
    public levelLoader sLevelLoader;
    public int nNextLevel;
    private void OnTriggerEnter2D(Collider2D other) {
        if(other.CompareTag("Player"))
        {
            sLevelLoader.LoadLevel(nNextLevel);
        }
    }
}
