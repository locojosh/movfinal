﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyGameManager : MonoBehaviour
{
    public static MyGameManager instance;

    [SerializeField] private Collectable_ScriptableObject[] collectables;
    public Collectable_ScriptableObject[] Collectables {get{return collectables;}}
    
    private void Awake() 
    {
        MakeSingleton();
    }
    private void MakeSingleton()
    {
        if(instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }
    
    public void AddCollectable(string typeName)
    {
        for (int i = 0; i < collectables.Length; i++)
        {
            if(collectables[i].Type.Equals(typeName))
            {
                collectables[i].AddToNumber();
            }
        }
        Player.Instance.UpdateUI();
    }
    public Collectable_ScriptableObject GetCollectable(string typeName)
    {
        for (int i = 0; i < collectables.Length; i++)
        {
            if(collectables[i].Type.Equals(typeName))
            {
                return collectables[i];
            }
        }
        return null;
    }

    #region SAVE/LOAD
    public void Save()
    {
        GameSaveLoad.Save(PlayerPrefs.GetInt("match"), "gmanager", this);
    }
    public void Load()
    {
        GameData data = GameSaveLoad.Load(PlayerPrefs.GetInt("match"), "gmanager");
        
        if(data == null)
        {
            for (int i = 0; i < collectables.Length; i++)
            {
                collectables[i].SetNumber(0);
            }
        }
        else
        {
            for (int i = 0; i < collectables.Length; i++)
            {
                collectables[i].SetNumber(data.collectablesNumber[i]);
            }            
        }
        Player.Instance.UpdateUI();
    }
    #endregion
}
